from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [
    path(r'level/', level, name='level'),
    path(r'level/List', level_list, name='levelList'),
    path(r'level/new', post_level, name='postLevel'),
    path(r'level/update', update_level, name='updateLevel')

]

urlpatterns += staticfiles_urlpatterns()
