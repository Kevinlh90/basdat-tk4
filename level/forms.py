from django import forms


class LevelForm(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
    }

    #hidden input
    old_key = forms.CharField(
        label='old_key', required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-white bb-white',
                'id': 'form-old-key',
                'name': 'old key',
                'type': 'text'
            }))

    nama_level = forms.CharField(
        label='nama_level', required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-white bb-white',
                'id': 'form-nama-level',
                'name': 'nama level',
                'type': 'text'
            }))

    minimum_poin = forms.IntegerField(
        label='minimum_poin', max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-white bb-white',
                'id': 'form-minimum-poin',
                'name': 'minimum_poin',
                'type': 'text'
            }))

    deskripsi = forms.CharField(
        label='deskripsi', max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-white bb-white',
                'id': 'form-deskripsi',
                'name': 'deskripsi',
                'type': 'text'
            }))