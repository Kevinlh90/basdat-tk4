from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import LevelForm
import psycopg2

# Create your views here.
connection = psycopg2.connect(user="dzrdxkcfdohvet",
                              password="a730c0cc7464d18b1028d568d301634ec6b5d47bcee25dbe938702ea358619ff",
                              host="ec2-54-235-193-0.compute-1.amazonaws.com",
                              port="5432",
                              database="d241r20m8gcd79")

def level(request):
    is_login = request.session["is_login"]
    usr_type = request.session["usr_type"]
    response = {'is_login': is_login, 'usr_type': usr_type}
    return render(request, 'level.html', response)

def post_level(request):
    form = LevelForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        nama_level = request.POST.get('nama_level', False)
        minimum_poin = request.POST.get('minimum_poin', False)
        deskripsi = request.POST.get('deskripsi', False)

        with connection.cursor() as cursor:
            cursor.execute("""INSERT INTO database_level VALUES ('%s', '%s', '%s');""" % (
            nama_level, minimum_poin, deskripsi))
            connection.commit()

    return HttpResponseRedirect('/level/')

def update_level(request):
    form = LevelForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        old_key = request.POST.get('old_key', False)
        nama_level = request.POST.get('nama_level', False)
        minimum_poin = request.POST.get('minimum_poin', False)
        deskripsi = request.POST.get('deskripsi', False)

        with connection.cursor() as cursor:
            cursor.execute("""UPDATE level SET nama_level = '%s' AND minimum_poin = '%s' AND deskripsi = '%s' WHERE nama_level = '%s';""" % (
                nama_level, minimum_poin, deskripsi, old_key))
            connection.commit()

    return HttpResponseRedirect('/level/List')

def level_list(request):
    is_login = request.session["is_login"]
    usr_type = request.session["usr_type"]
    response = {'is_login': is_login, 'usr_type': usr_type}
    return render(request, 'levelList.html', response)
