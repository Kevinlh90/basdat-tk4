from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from .forms import AdminForm, AnggotaForm
from validate_email import validate_email
from database.models import Admin, Anggota, Pengguna
import psycopg2

# Create your views here.
connection = psycopg2.connect(user = "dzrdxkcfdohvet",
                              password = "a730c0cc7464d18b1028d568d301634ec6b5d47bcee25dbe938702ea358619ff",
                              host = "ec2-54-235-193-0.compute-1.amazonaws.com",
                              port = "5432",
                              database = "d241r20m8gcd79")

def login(request):
	return render(request, 'login.html')

def logout(request):
    del request.session['no_ktp']
    del request.session['usr_type']
    del request.session['is_login']
    
    return redirect('/login/')

def admin(request):
    return render(request, 'admin.html', {'form': AdminForm()})

def anggota(request):
	return render(request, 'anggota.html',  {'form': AnggotaForm()})

def validate_form(request):
    no_ktp = request.GET.get('no_ktp', None)
    email = request.GET.get('email', None)
    
    if not no_ktp:
        no_ktp = -1
        
    with connection.cursor() as cursor:
        cursor.execute("""SELECT COUNT(1) FROM database_pengguna WHERE no_ktp = '%s';""" % (no_ktp))        
        v_no_ktp = cursor.fetchone()[0] == 0 #no_ktp tidak ada di database pengguna
        
    v_all = v_no_ktp and validate_email(email)
    
    data = {
        'is_available': v_no_ktp,
        'is_email_invalid': email and not validate_email(email),
        'is_submitable': v_all
    }
    
    return JsonResponse(data)

def validate_login(request):
    no_ktp = request.GET.get('no_ktp', None)
    email = request.GET.get('email', None)
    
    if not no_ktp:
        no_ktp = -1
        
    with connection.cursor() as cursor:
        usr_type = ''
        cursor.execute("""SELECT COUNT(1) FROM database_pengguna WHERE no_ktp = '%s';""" % (no_ktp))   
        v_no_ktp = cursor.fetchone()[0] == 1 #no_ktp ada di database pengguna
        if(v_no_ktp):
            cursor.execute("""SELECT COUNT(1) FROM database_admin WHERE no_ktp_id = '%s';""" % (no_ktp))
            usr_type = 'admin' if cursor.fetchone()[0] == 1 else 'anggota' #no_ktp ada di database admin atau anggota?
                
        cursor.execute("""SELECT COUNT(1) FROM database_pengguna WHERE no_ktp = '%s' AND email = '%s';""" % (no_ktp,email))
        v_email = cursor.fetchone()[0] == 1 #no_ktp memiliki email yang benar
            
    v_all = v_no_ktp and v_email
    
    data = {
        'is_available': v_no_ktp,
        'is_email_valid': v_email,
        'is_submitable': v_all,
        'usr_type': usr_type
    }
    
    return JsonResponse(data)

def logged_in(request):    
    no_ktp = request.GET.get('no_ktp', None)
    email = request.GET.get('email', None)
    usr_type = request.GET.get('usr_type', None)
    
    request.session['no_ktp'] = no_ktp
    request.session['usr_type'] = usr_type
    request.session['is_login'] = True
    
    return redirect('/index/')

def add_admin(request):
    form = AdminForm(request.POST or None)
    
    if(request.method == 'POST' and form.is_valid()):
        no_ktp = request.POST.get('no_ktp', False)
        nama_lengkap = request.POST.get('nama_lengkap', False)
        email = request.POST.get('email', False)
        tanggal_lahir = request.POST.get('tanggal_lahir', False)
        no_telp = request.POST.get('no_telp', False)
        
        with connection.cursor() as cursor:
            cursor.execute("""INSERT INTO database_pengguna VALUES ('%s', '%s', '%s', '%s', '%s');""" % (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp))            
            cursor.execute("""INSERT INTO database_admin VALUES ('%s');""" % (no_ktp))
            connection.commit()
        
    return HttpResponseRedirect('/login/')

def add_anggota(request):
    form = AnggotaForm(request.POST or None)
    
    if(request.method == 'POST' and form.is_valid()):
        no_ktp = request.POST.get('no_ktp', False)
        nama_lengkap = request.POST.get('nama_lengkap', False)
        email = request.POST.get('email', False)
        tanggal_lahir = request.POST.get('tanggal_lahir', False)
        no_telp = request.POST.get('no_telp', False)
        alamat = request.POST.get('alamat', False)
        
        with connection.cursor() as cursor:
            cursor.execute("""INSERT INTO database_pengguna VALUES ('%s', '%s', '%s', '%s', '%s');""" % (no_ktp,nama_lengkap,email,tanggal_lahir,no_telp))            
            cursor.execute("""INSERT INTO database_anggota VALUES ('%s', '%s');""" % (no_ktp,alamat))
            connection.commit()
        
    return HttpResponseRedirect('/login/')