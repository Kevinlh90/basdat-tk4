from django import forms          
        
class AdminForm(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
    }
    
    no_ktp = forms.IntegerField(
        label='no_ktp', required=True,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-no-ktp',
                'name': 'no ktp',
                'type':'text'
            }))
    
    nama_lengkap = forms.CharField(
        label='nama_lengkap', max_length=200,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-nama-lengkap',
                'name': 'nama lengkap',
                'type':'text'
            }))
    
    email = forms.CharField(
        label='email', max_length=200,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-email',
                'name': 'email',
                'type':'text'
            }))
    
    tanggal_lahir = forms.DateField(
        label='tanggal_lahir',
        widget = forms.DateInput(
            attrs= {
                'class': 'form-input text-white bb-white',
                'id': 'form-tanggal-lahir',
                'name': 'tanggal lahir'
            }))
    
    no_telp = forms.CharField(
        label='no_telp', max_length=20,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-no-telp',
                'name': 'nomor telepon',
                'type':'text'
            }))
    

class AnggotaForm(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
    }
    
    no_ktp = forms.IntegerField(
        label='no_ktp', required=True,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-no-ktp',
                'name': 'no ktp',
                'type':'text'
            }))
    
    nama_lengkap = forms.CharField(
        label='nama_lengkap', max_length=200,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-nama-lengkap',
                'name': 'nama lengkap',
                'type':'text'
            }))
    
    email = forms.CharField(
        label='email', max_length=200,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-email',
                'name': 'email',
                'type':'text'
            }))
    
    tanggal_lahir = forms.DateField(
        label='tanggal_lahir',
        widget = forms.DateInput(
            attrs= {
                'class': 'form-input text-white bb-white',
                'id': 'form-tanggal-lahir',
                'name': 'tanggal lahir'
            }))
    
    no_telp = forms.CharField(
        label='no_telp', max_length=20,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-no-telp',
                'name': 'nomor telepon',
                'type':'text'
            }))
    
    alamat = forms.CharField(
        widget = forms.Textarea(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-alamat',
                'name': 'alamat',
                'type':'textarea'
            }))