from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [  
    path(r'logout/', logout, name = 'logout'),
    path(r'login/', login, name = 'login'),
    path(r'login/admin/', admin, name = 'admin'),
    path(r'login/anggota/', anggota, name = 'anggota'),
    url(r'^login/$', login, name='login'),
    url(r'/logged_in/$', logged_in, name='logged_in'),
    url(r'/add_admin/$', add_admin, name='add_admin'),
    url(r'/add_anggota/$', add_anggota, name='add_anggota'),
    url(r'^ajax/validate_form/$', validate_form, name='validate_form'),
    url(r'^ajax/validate_login/$', validate_login, name='validate_login'),
]

urlpatterns += staticfiles_urlpatterns()