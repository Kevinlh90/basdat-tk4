#Potongan kode ini saya dapatkan dari Farhan Azmi

from django.db import connection

def fetch_all_query_as_dictionaries(cursor):
    """
    Returns all query result as a dictionary with each key as the queried column name.
    For instance, if you do "SELECT * from LEVEL_KEANGGOTAAN",
    it will return a list of dictionaries
    [{'nama_level': 'foo1', 'minimum_poin': 'bar1', 'deskripsi': 'baz1'},
    {'nama_level': 'foo2', 'minimum_poin': 'bar2', 'deskripsi': 'baz2'}, ...]
    """
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def do_raw_sql(sql_statements, fetch=False):
    with connection.cursor() as cursor:
        cursor.execute(sql_statements)
        if fetch:
            return fetch_all_query_as_dictionaries(cursor)
