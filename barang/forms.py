from django import forms
from utils.db_query_utils import do_raw_sql

#Beberapa potongan kode ini saya dapatkan dari Farhan Azmi

class BarangForm(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
    }
    
    id_barang = forms.CharField(
        label='id_barang', max_length=10, required=True,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-id-barang',
                'name': 'id_barang',
            }))
    
    nama_item = forms.ChoiceField(label="Nama Item",
                                  choices=[
                                      (item["nama"], item["nama"])
                                      for item in do_raw_sql("SELECT nama FROM database_item", fetch=True)
                                  ],
                                  required=True,
                                  widget=forms.TextInput(
                                    attrs = {
                                        'class': 'form-input text-white bb-white',
                                        'id': 'form-nama-item',
                                        'name': 'nama_item',

                                    }))

    warna = forms.CharField(
        label='warna', max_length=50,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-warna',
                'name': 'warna',
                'type':'text'
            }))
    
    url_foto = forms.URLField(
        label="URL Foto", required=False,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-url-foto',
                'name': 'url_foto',
            }))

    kondisi = forms.CharField(
        label="Kondisi", required=True,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-kondisi',
                'name': 'kondisi',
                'type':'text'
            }))

    lama_penggunaan = forms.IntegerField(
        label="Lama Penggunaan", min_value=0, required=False, initial=0,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-lama-penggunaan',
                'name': 'lama_penggunaan',
            }))

    owner = forms.ChoiceField(
        label="Pemilik/Penyewa",
                              choices=[
                                  (member["nama_lengkap"], member["nama_lengkap"])
                                  for member in do_raw_sql("SELECT nama_lengkap FROM database_pengguna "
                                                           "WHERE no_ktp IN "
                                                           "(SELECT no_ktp FROM database_anggota)", fetch=True)
                              ],
                              required=True,
                              widget=forms.TextInput(
                                    attrs = {
                                        'class': 'form-input text-white bb-white',
                                        'id': 'form-owner',
                                        'name': 'owner',
                                    }))

    
    royalti_bronze = forms.CharField(
        label='royalti_bronze', max_length=20,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-royalti-bronze',
                'name': 'royalti_bronze',
            }))

    harga_bronze = forms.IntegerField(
        label='harga_bronze',
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-harga-bronze',
                'name': 'harga_bronze',
            }))

    royalti_silver = forms.CharField(
        label='royalti_silver', max_length=20,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-royalti-silver',
                'name': 'royalti_silver',
            }))

    harga_silver = forms.IntegerField(
        label='harga_silver',
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-harga-silver',
                'name': 'harga_silver',
            }))

    royalti_gold = forms.CharField(
        label='royalti_gold', max_length=20,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-royalti-gold',
                'name': 'royalti_gold',
            }))

    harga_gold = forms.IntegerField(
        label='harga_gold',
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-harga-gold',
                'name': 'harga_gold',
            }))