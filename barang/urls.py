from django.urls import path, include
from .views import *

urlpatterns = [    
    path('', daftar_barang, name = 'daftar-barang'),
    path('validate', validate, name='validate'),
    path('validate-update', validate_update, name='validate-update'),
    path('add', add_barang, name='add-barang'),
    path('<str:id_barang>/delete_barang', delete_barang, name='delete-barang'),
    path('<str:id_barang>/update_barang', update_barang, name='update-barang'),
    path('<str:id_barang>', detail_barang, name='detail-barang'),
]