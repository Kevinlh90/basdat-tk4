from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import BarangForm
from utils.db_query_utils import do_raw_sql
import psycopg2

# Create your views here.
connection = psycopg2.connect(user = "dzrdxkcfdohvet",
                              password = "a730c0cc7464d18b1028d568d301634ec6b5d47bcee25dbe938702ea358619ff",
                              host = "ec2-54-235-193-0.compute-1.amazonaws.com",
                              port = "5432",
                              database = "d241r20m8gcd79")


def detail_barang(request, id_barang):
    if "no_ktp" in request.session.keys():
        with connection.cursor() as cursor:
            barang_object = cursor.execute("SELECT * FROM barang WHERE id_barang='{}'".format(id_barang))
            related_reviews = cursor.execute("SELECT tanggal_review, review FROM barang_dikirim WHERE id_barang='{}'"
                                     .format(id_barang))
        return render(request, 'barang_detail.html', context={"barang": barang_object[0], "reviews": related_reviews})
    else:
        return HttpResponseRedirect(reverse("login"))

def validate(request):
    id_barang = request.GET.get('id_barang', None)
    
    with connection.cursor() as cursor:
        cursor.execute("""SELECT COUNT(1) FROM barang WHERE id_barang = '%s';""" % (id_barang))        
        v_nama = cursor.fetchone()[0] == 0 #nama tidak ada di database barang
    
    data = {
        'is_available': v_nama
    }
    
    return JsonResponse(data)

def validate_update(request):
    old_id_barang = request.GET.get('old_id_barang', None)
    id_barang = request.GET.get('id_barang', None)
    
    if(id_barang != old_id_barang):
        with connection.cursor() as cursor:
            cursor.execute("""SELECT COUNT(1) FROM barang WHERE id_barang = '%s';""" % (id_barang))
            v_nama = cursor.fetchone()[0] == 0 #nama tidak ada di database barang
    else:
        v_nama = True
    
    data = {
        'is_available': v_nama
    }
    
    return JsonResponse(data)

def daftar_barang(request):
    # TODO REMOVE THESE ONCE MERGED TO MASTER
    request.session["no_ktp"] = "ancol"
    request.session["type"] = "admin"
    if "no_ktp" in request.session.keys():
        return render(request, 'barang_list.html')
    else:
        return HttpResponseRedirect(reverse("login"))


def add_barang(request):
    # TODO cek session role user yang login
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        level_barang = do_raw_sql("SELECT nama_level FROM level_keanggotaan", fetch=True)
        if request.method == "POST":
            form = BarangForm(request.POST)

            if(request.method == 'POST'):
                id_barang = request.POST.get('id_barang', False)
                owner_name = request.POST.get('owner', False)
                royalti_bronze = request.POST.get('royalti_bronze', False)
                harga_bronze = request.POST.get('harga_bronze', False)
                royalti_silver = request.POST.get('royalti_silver', False)
                harga_silver = request.POST.get('harga_silver', False)
                royalti_gold = request.POST.get('royalti_gold', False)
                harga_gold = request.POST.get('harga_bronze', False)
                owner_object = do_raw_sql("SELECT no_ktp FROM pengguna WHERE nama_lengkap='{}'".format(owner_name),
                                          fetch=True)

            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO barang VALUES ('{}', '{}', '{}', '{}', '{}', {}, '{}')".format(
                    id_barang, request.POST["nama_item"], request.POST["warna"],
                    request.POST["url_foto"], request.POST["kondisi"], request.POST["lama_penggunaan"],
                    owner_object[0]["no_ktp"])
                    )
                cursor.execute("INSERT INTO info_barang_level VALUES ('{}', 'bronze', {}, {})"
                                   .format(id_barang, harga_bronze, royalti_bronze)
                    )
                cursor.execute("INSERT INTO info_barang_level VALUES ('{}', 'silver', {}, {})"
                                   .format(id_barang, harga_silver, royalti_silver)
                    )
                cursor.execute("INSERT INTO info_barang_level VALUES ('{}', 'gold', {}, {})"
                                   .format(id_barang, harga_gold, royalti_gold)
                    )
                connection.commit()

                return HttpResponseRedirect(reverse('get-daftar'))

        else:
            form = BarangForm()

def update_barang(request, id_barang):
    # TODO cek session role user yang login
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        level_barang = do_raw_sql("SELECT nama_level FROM level_keanggotaan", fetch=True)
        if request.method == "POST":
            form = BarangForm(request.POST)
    
            if(request.method == 'POST'):
                id_barang = request.POST.get('id_barang', False)
                owner_name = request.POST.get('owner', False)
                royalti_bronze = request.POST.get('royalti_bronze', False)
                harga_bronze = request.POST.get('harga_bronze', False)
                royalti_silver = request.POST.get('royalti_silver', False)
                harga_silver = request.POST.get('harga_silver', False)
                royalti_gold = request.POST.get('royalti_gold', False)
                harga_gold = request.POST.get('harga_bronze', False)
                owner_object = do_raw_sql("SELECT no_ktp FROM pengguna WHERE nama_lengkap='{}'".format(owner_name),
                                          fetch=True)
            
            with connection.cursor() as cursor:
                cursor.execute("UPDATE barang SET nama_item='{}', warna='{}', url_foto='{}', kondisi='{}', "
                        "lama_penggunaan='{}', no_ktp_penyewa='{}' WHERE id_barang='{}'".format(
                            form.cleaned_data["nama_item"], form.cleaned_data["warna"], form.cleaned_data["url_foto"],
                            form.cleaned_data["kondisi"], form.cleaned_data["lama_penggunaan"], owner_object[0]["no_ktp"],
                            id_barang)
                    )
                cursor.execute("UPDATE info_barang_level SET porsi_royalti={}, harga_sewa={} "
                                "WHERE id_barang='{}' AND nama_level='bronze'".format(
                                royalti_bronze, harga_bronze, id_barang)
                    )
                cursor.execute("UPDATE info_barang_level SET porsi_royalti={}, harga_sewa={} "
                                "WHERE id_barang='{}' AND nama_level='silver'".format(
                                royalti_silver, harga_silver, id_barang)
                    )
                cursor.execute("UPDATE info_barang_level SET porsi_royalti={}, harga_sewa={} "
                                "WHERE id_barang='{}' AND nama_level='gold'".format(
                                royalti_gold, harga_gold, id_barang)
                    )
                connection.commit()
    
                return HttpResponseRedirect(reverse('get-daftar'))
    
        else:
            form = BarangForm()
    

def delete_barang(request, id_barang):
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        with connection.cursor() as cursor:
            cursor.execute("DELETE from barang WHERE id_barang='{}'".format(id_barang))            
            return HttpResponseRedirect(reverse("list-barang"))
    else:
        return HttpResponseRedirect(reverse("login"))
