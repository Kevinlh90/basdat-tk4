from django import forms          
        
class ItemForm(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
    }
    
    nama = forms.CharField(
        max_length=200,
        required=True,
        widget=forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-nama',
                'name': 'nama',
                'type': 'text'
            }))
    
    deskripsi = forms.CharField(
        widget = forms.Textarea(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-deskripsi',
                'name': 'deskripsi',
                'type': 'textarea'
            }))
    
    rentang_usia = forms.CharField(
        max_length=200,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-rentang-usia',
                'name': 'rentang-usia',
                'type': 'text'
            }))
    
    bahan = forms.CharField(
        max_length=200,
        widget = forms.TextInput(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-bahan',
                'name': 'bahan',
                'type': 'text'
            }))
    
    kategori = forms.CharField(
        widget = forms.Textarea(
            attrs = {
                'class': 'form-input text-white bb-white',
                'id': 'form-kategori',
                'name': 'kategori',
                'type': 'textarea'
            }))