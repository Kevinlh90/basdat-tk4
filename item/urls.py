from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [  
    path(r'item/', item, name = 'item'),
    path(r'item/create/', create_item, name = 'create_item'),
    url(r'^validate_item/$', validate_item, name='validate_item'),
    url(r'^validate_item_update/$', validate_item_update, name='validate_item_update'),
    url(r'/add_item/$', add_item, name='add_item'),
    url(r'^get_item/$', get_item, name='get_item'),
    url(r'^update_item/$', update_item, name='update_item'),
    url(r'^delete_item/$', delete_item, name='delete_item'),
    url(r'^item/$', item, name='item'),
    url(r'^item/update/(?P<nama>[-\w]+)/$', update_item_page, name='update_item_page'),
]

urlpatterns += staticfiles_urlpatterns()