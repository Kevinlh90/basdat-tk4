from django.http import HttpResponseRedirect, JsonResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from .forms import ItemForm
from database.models import Item
import psycopg2

# Create your views here.
connection = psycopg2.connect(user = "dzrdxkcfdohvet",
                              password = "a730c0cc7464d18b1028d568d301634ec6b5d47bcee25dbe938702ea358619ff",
                              host = "ec2-54-235-193-0.compute-1.amazonaws.com",
                              port = "5432",
                              database = "d241r20m8gcd79")

def item(request):
	return render(request, 'item.html', {'nbar': 'item', 'form': ItemForm()})

def create_item(request):
	return render(request, 'create_item.html', {'form': ItemForm()})

def validate_item(request):
    nama = request.GET.get('nama', None)
    
    with connection.cursor() as cursor:
        cursor.execute("""SELECT COUNT(1) FROM database_item WHERE nama = '%s';""" % (nama))        
        v_nama = cursor.fetchone()[0] == 0 #nama tidak ada di database item
    
    data = {
        'is_available': v_nama
    }
    
    return JsonResponse(data)

def validate_item_update(request):
    old_nama = request.GET.get('old_nama', None)
    nama = request.GET.get('nama', None)
    
    if(nama != old_nama):
        with connection.cursor() as cursor:
            cursor.execute("""SELECT COUNT(1) FROM database_item WHERE nama = '%s';""" % (nama))
            v_nama = cursor.fetchone()[0] == 0 #nama tidak ada di database item
    else:
        v_nama = True
    
    data = {
        'is_available': v_nama
    }
    
    return JsonResponse(data)

def add_item(request):
    form = ItemForm(request.POST or None)
    
    if(request.method == 'POST' and form.is_valid()):
        nama = request.POST.get('nama', False)
        deskripsi = request.POST.get('deskripsi', False)
        rentang_usia = request.POST.get('rentang_usia', False)
        bahan = request.POST.get('bahan', False)
        kategori = request.POST.get('kategori', False)
        
        with connection.cursor() as cursor:
            cursor.execute("""INSERT INTO database_item VALUES ('%s', '%s', '%s', '%s', '%s');""" % (nama,deskripsi,rentang_usia,bahan,kategori))
            connection.commit()
        
    return HttpResponseRedirect('/item/')

def get_item(request):
    with connection.cursor() as cursor:
        cursor.execute("""SELECT * FROM database_item""")
        data = cursor.fetchall()
    return JsonResponse(data, safe=False)

def delete_item(request):
    nama = request.GET.get('nama', None)
    
    with connection.cursor() as cursor:
        cursor.execute("""DELETE FROM database_item WHERE nama = '%s';""" % (nama))
        connection.commit()
    return JsonResponse({})

def update_item(request):
    old_nama = request.GET.get('old_nama', None)
    nama = request.GET.get('nama', None)
    deskripsi = request.GET.get('deskripsi', None)
    rentang_usia = request.GET.get('rentang_usia', None)
    bahan = request.GET.get('bahan', None)
    kategori = request.GET.get('kategori', None)
    
    with connection.cursor() as cursor:
        cursor.execute("""UPDATE database_item SET nama = '%s', deskripsi = '%s', rentang_usia = '%s', bahan = '%s', kategori = '%s' WHERE nama = '%s';""" % (nama,deskripsi,rentang_usia,bahan,kategori,old_nama))
        connection.commit()
        
    return HttpResponseRedirect('/item/')

def update_item_page(request, nama):
    with connection.cursor() as cursor:
        cursor.execute("""SELECT * FROM database_item WHERE nama = '%s'""" % (nama))
        data = cursor.fetchone()
        
    if(data is None):
        return HttpResponseNotFound("page not found")
    
    return render(request, 'update_item.html', {'data':data})