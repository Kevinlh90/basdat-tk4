from django.shortcuts import render

# Create your views here.
def index(request):
    if(not 'is_login' in request.session):
        request.session['is_login'] = False
        request.session['usr_type'] = 'Guest'
    response = {
        'nbar': 'index'
    }
    return render(request, 'index.html', response)