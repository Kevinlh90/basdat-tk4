from django.db import models

# Create your models here.    
class Pengguna(models.Model):
    no_ktp = models.IntegerField(primary_key = True)
    nama_lengkap = models.CharField(max_length = 200)
    email = models.CharField(max_length = 200)
    tanggal_lahir = models.DateField()
    no_telp = models.CharField(max_length = 20)
    
    def __str__(self):
        return str(self.no_ktp)
    
class Admin(models.Model):
    no_ktp = models.ForeignKey(Pengguna, on_delete=models.CASCADE, unique = True, primary_key = True)
    
class Anggota(models.Model):
    no_ktp = models.ForeignKey(Pengguna, on_delete=models.CASCADE, unique = True, primary_key = True)
    alamat = models.TextField(null = True)
    
class Item(models.Model):
    nama = models.CharField(max_length = 200, primary_key = True)
    deskripsi = models.TextField(null = True)
    rentang_usia = models.CharField(max_length = 200, null = True)
    bahan = models.CharField(max_length = 200, null = True)
    kategori = models.TextField(null = True)
    
class Barang(models.Model):
    id_barang = models.IntegerField(primary_key = True)
    nama_item = models.CharField(max_length = 200)
    warna = models.CharField(max_length = 50)
    url_foto = models.CharField(max_length = 500)
    kondisi = models.CharField(max_length = 50)
    lama_penggunaan = models.IntegerField()
    pemilik = models.CharField(max_length = 200)
    
    level = models.CharField(max_length = 50)
    royalty = models.IntegerField()
    harga_sewa = models.IntegerField()