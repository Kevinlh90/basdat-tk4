from django.contrib import admin
from database.models import Pengguna, Admin, Anggota, Item, Barang

# Register your models here.
class PenggunaModelAdmin(admin.ModelAdmin):
    list_display = ["no_ktp", "email"]
    list_filter = ["tanggal_lahir"]
    class Meta:
        model = Pengguna
        
class AdminModelAdmin(admin.ModelAdmin):
    list_display = ["no_ktp"]
    class Meta:
        model = Admin
        
class AnggotaModelAdmin(admin.ModelAdmin):
    list_display = ["no_ktp", "alamat"]
    class Meta:
        model = Anggota
        
class ItemModelAdmin(admin.ModelAdmin):
    list_display = ["nama", "deskripsi"]
    class Meta:
        model = Item
        
admin.site.register(Pengguna, PenggunaModelAdmin)
admin.site.register(Admin, AdminModelAdmin)
admin.site.register(Anggota, AnggotaModelAdmin)
admin.site.register(Item, ItemModelAdmin)
admin.site.register(Barang)