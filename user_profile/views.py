from django.http import HttpResponseRedirect, JsonResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
import psycopg2

# Create your views here.
connection = psycopg2.connect(user = "dzrdxkcfdohvet",
                              password = "a730c0cc7464d18b1028d568d301634ec6b5d47bcee25dbe938702ea358619ff",
                              host = "ec2-54-235-193-0.compute-1.amazonaws.com",
                              port = "5432",
                              database = "d241r20m8gcd79")

def user_profile(request):
    usr_type = request.session['usr_type']
    no_ktp = request.session['no_ktp']
    with connection.cursor() as cursor:
        cursor.execute("""SELECT * FROM database_pengguna WHERE no_ktp = '%s'""" % (no_ktp))
        data = cursor.fetchone()
        
        if(usr_type == 'anggota'):
            cursor.execute("""SELECT alamat FROM database_anggota WHERE no_ktp_id = '%s';""" % (no_ktp))
            data = data + cursor.fetchone()
        
    if(data is None):
        return HttpResponseNotFound("page not found")
    
    response = {
        'nbar': 'user_profile',
        'data': data,
        'usr_type': usr_type
    }
    
    return render(request, 'user_profile.html', response)