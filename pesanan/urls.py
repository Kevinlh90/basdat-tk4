from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from .views import *

urlpatterns = [  
    path(r'pesanan/', pesanan, name='pesanan'),
    path(r'pesanan/list', pesanan_list, name='pesananList'),
    path(r'pesanan/new', post_pesanan, name ='postPesanan'),
    path(r'pesanan/update', update_pesanan, name='updatePesanan')
]

urlpatterns += staticfiles_urlpatterns()
