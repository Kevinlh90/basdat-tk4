from django import forms


class PesananForm(forms.Form):
    error_messages = {
        'required': 'This field is required',
        'invalid': 'is not valid',
    }

    anggota = forms.CharField(
        label='anggota', required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-white bb-white',
                'id': 'form-anggota',
                'name': 'anggota',
                'type': 'text'
            }))

    barang = forms.CharField(
        label='barang', max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-white bb-white',
                'id': 'form-barang',
                'name': 'barang',
                'type': 'text'
            }))

    lama_sewa = forms.IntegerField(
        label='lama_sewa', max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'form-input text-white bb-white',
                'id': 'lama_sewa',
                'name': 'lama_sewa',
                'type': 'text'
            }))