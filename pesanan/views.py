from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import PesananForm
import psycopg2

# Create your views here.
connection = psycopg2.connect(user="dzrdxkcfdohvet",
                              password="a730c0cc7464d18b1028d568d301634ec6b5d47bcee25dbe938702ea358619ff",
                              host="ec2-54-235-193-0.compute-1.amazonaws.com",
                              port="5432",
                              database="d241r20m8gcd79")

def pesanan(request):
    is_login = request.session["is_login"]
    usr_type = request.session["usr_type"]
    response = {'is_login': is_login, 'usr_type': usr_type}
    return render(request, 'pesanan.html', response)

def post_pesanan(request):
    form = PesananForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        anggota = request.POST.get('anggota', False)
        barang = request.POST.get('barang', False)
        lama_sewa = request.POST.get('lama_sewa', False)

        with connection.cursor() as cursor:
            cursor.execute("""INSERT INTO database_pesanan VALUES ('%s', '%s', '%s');""" % (
            anggota, barang, lama_sewa))
            connection.commit()

    return HttpResponseRedirect('/pesanan/')

def update_pesanan(request):
    form = PesananForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        anggota = request.POST.get('anggota', False)
        barang = request.POST.get('barang', False)
        lama_sewa = request.POST.get('lama_sewa', False)

        with connection.cursor() as cursor:
            cursor.execute("""UPDATE level SET nama_level = '%s' AND minimum_poin = '%s' AND deskripsi = '%s' WHERE nama_level = '%s';""" % (
                anggota, barang, lama_sewa))
            connection.commit()

    return HttpResponseRedirect('/pesanan/')

def pesanan_list(request):
    is_login = request.session["is_login"]
    usr_type = request.session["usr_type"]
    response = {'is_login': is_login, 'usr_type': usr_type}
    return render(request, 'pesananList.html', response)
